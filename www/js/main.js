var app = angular.module('stuffs', ['ui.router',
												 'ionic', 
												 'ng-token-auth',
												 'starter.controllers', 
												 'starter.services']);

	
var _g_serverAddr = 'http://ventory.herokuapp.com';
app.value('g_serverAddr', _g_serverAddr);											 

app.run(function($ionicPlatform) {
	
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)

    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
	
	function checkConnection() {
		var networkState = navigator.connection.type;
		var states = {};
		states[Connection.UNKNOWN]  = 'Unknown connection';
		states[Connection.ETHERNET] = 'Ethernet connection';
		states[Connection.WIFI]     = 'WiFi connection';
		states[Connection.CELL_2G]  = 'Cell 2G connection';
		states[Connection.CELL_3G]  = 'Cell 3G connection';
		states[Connection.CELL_4G]  = 'Cell 4G connection';
		states[Connection.CELL]     = 'Cell generic connection';
		states[Connection.NONE]     = 'No network connection';

		console.log('Connection type: ' + states[networkState]);
		
		return networkState;
	}

	if (checkConnection() == Connection.NONE) {
		alert("NO Internet Connection, \ncome back again when you have internet connectivity. Thanks!");
		ionic.Platform.exitApp();
	}
  })
});

app.config(["$httpProvider", "$authProvider", function($httpProvider, $authProvider) {

		$authProvider.configure({
		            apiUrl: _g_serverAddr
		});

		var serverAddr = _g_serverAddr;

		$httpProvider.defaults.withCredentials = true;
		$httpProvider.defaults.useXDomain = true;
		$httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
  }
]);

	