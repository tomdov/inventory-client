app.controller("signupController", function($scope, $rootScope, $state, $auth) {
		
		$scope.show_loading = false;
		
		$scope.signup_user = function(params) {
		
			$scope.show_loading = true; 
			
			var credentials = {
				name: params.name,
				email: params.email,
				password: params.password,
				password_confirmation: params.password_confirmation
			};
			
			$auth.submitRegistration(credentials).then(function(response) {
				console.log('User Registered');
				console.log(response); // => {id: 1, ect: '...'}
				
				}, function(error) {
					// Registration failed...
					$scope.handle_error_data(error.data);
					$scope.reset_params_after_failure();
			});
					
		};
		
		$scope.handle_error_data = function(error_data) {
			var error_message = "";
			for (var key in error_data){
				error_message += key + ": " + error_data[key] + "\n";
			}
			
			alert('Error happened during sign up:\n\n' + error_message);
        };
		
		$scope.reset_params_after_failure = function() {
			$scope.show_loading = false;
		};

        $scope.$on('auth:registration-email-success', function(event, user) {
            console.log(event + 'email sent to ' + user.email);
            alert('Confirmation mail sent to ' + user.email + '\nplease confirm your mail and then signin');      
			$scope.user.password = "";
			$state.go('signin');
        });

        $scope.$on('auth:registration-email-error', function(event, user) {
        	alert('sending mail to ' + user.email + ' failed');
        });
		
});