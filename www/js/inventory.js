
app.controller("inventoryController", function($scope, $http, $state, $timeout, g_serverAddr, $auth, $rootScope) {
	  
	/* INITIALIZE */
	$scope.elements = [];
	$scope.newItemMode = "new";
	$scope.newItem = {};
	$scope.pendingItem = {};
	$scope.itemTitle = "New Item";
	$scope.current_user_id = 0;
	$scope.inventoryListLoaded = false;
	$scope.disableSaveList = false;
	$scope.serverAddr = g_serverAddr;
	$scope.inventoryListSaving = false;
	$scope.InventoryListSaved = false;

	$(function() {
		$(".date_picker").datepicker({ dateFormat: "dd/mm/yy", showButtonPanel: true });
	});

	$auth.validateUser().then(
		function(user) {
			console.log('validation success (inventorylist) promise');
			$scope.onValidationSuccess(user);
		}, 
		function(error) {
			console.log('validation error (inventorylist) promise');
			$scope.onValidationFailed();
		}
	);

	/* LISTENERS */
	
	$rootScope.$on('auth:validation-error', function(event, user) {
		console.log('validation error (inventorylist) event=' + event + ' user=' + user);
		$scope.onValidationFailed();
	});	

	$rootScope.$on('auth:validation-success', function(event, user) {
		console.log('validation success (inventorylist) event=' + event + ' user=' + user);
		$scope.onValidationSuccess();
	});	

	$scope.$on('itemPassing', function(event, item) {
        console.log('Item added to the invnetory!');
		var tempItem = {
			description: item.description,
			qty: item.qty,
			reminderDate: "",
			notes: "",
			price: item.price
		};

		$scope.addItem(tempItem, true);
    });

    /* FUNCTIONS */

  	$scope.onValidationSuccess = function(user) {
		if (user) {
			$scope.current_user_id = user.id;
			$scope.fetchRemoteInventoryList($scope.current_user_id);
		}
	};

	$scope.onValidationFailed = function() {
		$state.go('signin');
	}

	$scope.removeListSavedMessage = function () {
		$scope.InventoryListSaved = false;
	};
	
	$scope.fetchRemoteInventoryList = function (user_id) {
		
		$http({method: 'GET', url: $scope.serverAddr + '/users/' + $scope.current_user_id + '/inventory_list', withCredentials: true}).success(
			function(data, status, headers, config) {
					$scope.resetErrorMessages();
					$scope.elements = angular.fromJson(data); 
					$scope.inventoryListLoaded = true;
			}					
		).error(
			function() { 
				console.log("error detected when trying to fetch user's inventory list");
			}
		);	
	
	};

	$scope.saveRemoteInventoryList = function () {
		
		$scope.disableSaveList = true;
		$scope.inventoryListSaving = true;
		
		var data = { id : $scope.current_user_id,
						list : angular.toJson($scope.elements) };
		$http.post($scope.serverAddr + '/inventory_lists', data).success(
			function(data, status, headers, config) {
				$scope.resetErrorMessages();
				$scope.disableSaveList = false;
				$scope.inventoryListSaving = false;
				$scope.InventoryListSaved = true;
				console.log("inventory list saved for user " + $scope.current_user_id); 
				$timeout(function() { $scope.removeListSavedMessage(); }, 5000);
		}).error(
			function() { 
				$scope.errorSaveList = true;
				$scope.disableSaveList = false;
				$scope.inventoryListSaving = false;
			}
		);		
	};	

	$scope.addItem = function(newItem, fromShoppingList) {
		if ($scope.newItemMode == "new" || fromShoppingList) {
			$scope.elements.push(newItem);
		}
		$scope.newItem = {};
		$scope.newItemMode = "new";
		$scope.itemTitle = "New Item";
		$scope.disableSaveList = false;
		$scope.saveRemoteInventoryList();
	};
	
	$scope.editItem = function(item) {
		$scope.newItem = item;
		$scope.newItemMode = "edit";
		$scope.itemTitle = "Edit Item";
		$scope.disableSaveList = true;
	};
	
	$scope.returnElementsWithoutItem = function(elements, item) {
		return elements.filter(function(elem){ 
			return (elem.description != item.description && elem.qty != item.qty)
		});
	};
	
	$scope.sendItemToShoppingList = function(item) {
		$rootScope.$broadcast('itemPassingToShoppingList', item);
	};
	
	$scope.closeMoveToShoppingListReveal = function() {
		$('#passToShoppingListModal').foundation('reveal', 'close');
	};
	
	$scope.removeItemFromInventory = function(item) {
		$scope.elements = $scope.returnElementsWithoutItem($scope.elements, item);
	};
	
	$scope.getPendingItem = function() {
		return $scope.pendingItem;
	};
	
	$scope.setPendingItem = function(item) {
		$scope.pendingItem = item;
	};
	
	$scope.sendItemToShoppingListPrepare = function(item) {
		$scope.setPendingItem(item);
	};
	
	$scope.sendItemToShoppingListDo = function(moveToShoppingList) {
		var tempItem = $scope.getPendingItem();
		if (moveToShoppingList == 'yes') {
			$scope.sendItemToShoppingList(tempItem);
		}
		$scope.removeItemFromInventory(tempItem);
		$scope.saveRemoteInventoryList();
		$scope.closeMoveToShoppingListReveal();
	};
	
	$scope.resetErrorMessages = function() {
		$scope.errorSaveList = false;
	};

} );

  
