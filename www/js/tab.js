app.controller("tabController", function($scope, $state, $http, g_serverAddr, $auth) {
		
	$scope.current_user_id = 0;
		
	$scope.serverAddr = g_serverAddr;
	
	$scope.selectedTab = 1;
	
	$scope.setTab = function(tab) {
		$scope.selectedTab = tab;
	};
	
	$scope.isSelectedTab = function(tab) {
		return $scope.selectedTab === tab;
	};
	
	$scope.logout = function() {
		$auth.signOut().then(function(oldUser) {
			console.log(oldUser + "have signed out now.");
       }, function(error) {
          // An error occurred logging out.
      });
	};
	
} );
