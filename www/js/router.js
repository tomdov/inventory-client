app.config(function ($stateProvider, $urlRouterProvider) {
		
        $stateProvider
            .state('start',{
                url:"/",
                templateUrl:"main.htm"
            })
            .state('inventory',{
                url:"/main",
                templateUrl:"mainInventory.htm"
            })
			.state('signup',{
				url:"/users/new",
				templateUrl:"signup.htm"
			})
			.state('signin',{
				url:"/signin",
				templateUrl:"login.htm"
			})
			.state('shoppingList',{
				url:"/#/panel1",
				templateUrl:"shopping.htm"
			})
			.state('inventoryList',{
				url:"/#/panel2",
				templateUrl:"inventory.htm"
			})
			
		// if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('main');
});