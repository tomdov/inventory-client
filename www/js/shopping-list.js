app.controller("shoppingListController", function($http, $log, $scope, $state, $timeout, g_serverAddr, $rootScope, $auth) {
	
	/* INITIALIZE */
	$scope.current_user_id = 0;
	$scope.shoppingListLoaded = false;
	$scope.elements = [];
	$scope.newItem = {};
	$scope.disableSaveList = false;
	$scope.shoppingListIsLoading = false;
	$scope.shoppingListSaved = false;
	$scope.serverAddr = g_serverAddr;
	$scope.shoppingListSavedError = false;

	$auth.validateUser().then(
		function(user) {
			console.log('validation success (shopping) user=' + user);
			$scope.current_user_id = user.id;
			$scope.fetchRemoteShoppingList($scope.current_user_id);
		}, 
		function(error) {
			console.log('validation error (shoppinglist) error=' + error);
			$state.go('signin');
		}
	);

	/* LISTENERS */

	$rootScope.$on('auth:validation-error', function(event, user) {
		console.log('validation error (shoppinglist) event=' + event + ' user=' + user);
		$scope.onValidationFailed();
	});	

	$rootScope.$on('auth:validation-success', function(event, user) {
		console.log('validation success (shopping) event=' + event + ' user=' + user);
		$scope.onValidationSuccess(user);
	});	

	$scope.$on('itemPassingToShoppingList', function(event, item) {
		var tempItem = {
			description: item.description,
			qty: item.qty,
			price: item.price
		};

		$scope.addItem(tempItem);
    });

	
	$scope.$on('itemPassingFromInventory', function(event, item) {
		var tempItem = {
			description: item.description,
			qty: item.qty,
			price: item.price
		};

		$scope.addItem(tempItem);
    });

    /* FUNCTIONS */

	$scope.addItem = function(newItem) {
		$scope.elements.push(newItem);
		$scope.newItem = {};
		$scope.disableSaveList = false;
		$scope.saveRemoteShoppingList();
	};

	$scope.removeItem = function(item) {
		$scope.elements = $scope.elements.filter(function(elem){ 
			return elem.description != item.description 
		});
	};

	$scope.onValidationSuccess = function(user) {
		if (user) {
			$scope.current_user_id = user.id;
			$scope.fetchRemoteShoppingList($scope.current_user_id);
		}
	};

	$scope.onValidationFailed = function() {
		$state.go('signin');
	};
	
	$scope.fetchRemoteShoppingList = function (user_id) {
		
		$http({ method: 'GET', url: $scope.serverAddr + '/users/' + $scope.current_user_id + '/shopping_list', withCredentials: true }).success(
			function(data, status, headers, config) {
					$scope.elements = angular.fromJson(data); 
					$scope.shoppingListLoaded = true;
			}					
		).error(
			function() { alert('ERROR') }
		);	
	
	};

	$scope.removeListSavedMessage = function () {
		$scope.shoppingListSaved = false;
    };

	$scope.saveRemoteShoppingList = function () {
		
		var data = { id : $scope.current_user_id,
						list : angular.toJson($scope.elements) };

		//show the loading sign
		$scope.shoppingListIsLoading = true;

		$http.post($scope.serverAddr +  '/shopping_lists', data
			).success(
			function(data, status, headers, config) {
					console.log("shopping list saved for user " + $scope.current_user_id); 
					$scope.shoppingListIsLoading = false;
					$scope.shoppingListSaved = true;
					$scope.shoppingListSavedError = false;
					$timeout(function() { $scope.removeListSavedMessage(); }, 5000);
			}).error(
			function() { 
				$scope.shoppingListIsLoading = false;
				$scope.shoppingListSavedError = true;
				console.log('ERROR in saving shopping list');
			}
		);	
	
	};	
	
	$scope.prepareItemToInventory = function(desc, qty, reminderDate, notes, price) {
		
		var tempInventoryItem = [{
			description: desc,
			qty: qty,
			reminderDate: reminderDate,
			notes: notes,
			price: price
			}];  
		return tempInventoryItem;
	};
	
	$scope.sendItemPrepare = function(item) {
		$rootScope.$broadcast('itemPassing', item);
		$scope.elements = $scope.elements.filter(function(elem){ 
			return elem.description != item.description 
		});

		$scope.saveRemoteShoppingList();
	};
		
	$scope.closeMoveToInventoryListReveal = function() {
		$('#passToInventoryListModal').foundation('reveal', 'close');
	};
} );
