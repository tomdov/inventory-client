app.controller("loginController", function($scope, $rootScope, $state, $http, $auth) {

		$scope.signin_general_error = false;
		$scope.user_pressed_submit = false;
		$scope.signin_username_password_error = false;
		$scope.signin_user_is_not_authenticated = false;
		$scope.show_loading = false;
		$scope.signin_errors = "";
					
		$scope.login_user = function(params) {
		
			$scope.show_loading = true;
			var credentials = {
				email: params.email,
				password: params.password
			};

			$auth.submitLogin(credentials);					
		};


		$rootScope.$on('auth:login-success', function(event, user) {
			console.log('login succeeded for ' + user.email);
			$state.go('inventory');
		});

		$rootScope.$on('auth:login-error', function(event, reason) {
			console.log('login failed');
			$scope.reset_parameters_after_login_fail();
			$scope.signin_errors = reason.errors[0];				
		});

		
		$scope.reset_parameters_after_login_fail = function() {
			$scope.reset_error_flags();
			$scope.user_pressed_submit = false;
			$scope.show_loading = false;
			$scope.signin_errors = "";
		};
		
		$scope.reset_error_flags = function() {
			$scope.signin_general_error = false;
			$scope.signin_username_password_error = false;
			$scope.signin_user_is_not_authenticated = false;
		};

});